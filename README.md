# kernel-rt

CentOS real time kernel RPMs.

To build a new version of the RPMs:

  - update the TAG variable in the build.sh script
  - you can apply patches by putting them in the patches directory
  - if patches have to be applied, update the BUILDID in the build.sh script
  - push your changes to check that the RPMs can be built by gitlab-ci
  - tag the repository and push the tag for the RPMs to be uploaded to artifactory rpm-ics repo


Note that you should check which flags can be used in the `SPECS/kernel-rt.spec`.
The `%bcond_with    firmware` disappeared in the past for example.
