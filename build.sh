#!/bin/bash

# Define the tag to use
TAG="imports/c7-rt/kernel-rt-3.10.0-1062.12.1.rt56.1042.el7"
# WARNING! To update if new patches are applied
BUILDID=".ESS1"

git clone https://git.centos.org/git/centos-git-common.git
git clone https://git.centos.org/git/rpms/kernel-rt

cd kernel-rt
git checkout ${TAG} && ../centos-git-common/get_sources.sh -b c7-rt

nb_patches=$(ls ../patches | wc -l)
if [[ "$nb_patches" != "0" ]]
then
  echo "Copy patches and modify kernel-rt.spec to apply them"
  cp ../patches/*.patch SOURCES/
  cp -p SPECS/kernel-rt.spec SPECS/kernel-rt.spec.org
  for patch in $(ls ../patches/)
  do
    sed -i "/## Apply Patches here/a ApplyPatch ${patch}" SPECS/kernel-rt.spec
  done
  sed -i "/%define buildid/a %define buildid ${BUILDID}" SPECS/kernel-rt.spec

  ## Temporary patch for imports/c7-rt/kernel-rt-3.10.0-514.26.1.rt56.442.el7
  ## pkg_release includes buildid
  ## pkg_release_simple doesn't include el7
  #sed -i 's/Source0: %{name}-%{rpmversion}-%{pkg_release}.tar.xz/Source0: kernel-rt-3.10.0-514.26.1.rt56.442.el7.tar.xz/' SPECS/kernel-rt.spec

  diff -u SPECS/kernel-rt.spec.org SPECS/kernel-rt.spec
else
  echo "No patch to apply"
fi

rpmbuild --define "%_topdir $(pwd)" --target $(uname -m) --without debug --without trace -ba SPECS/kernel-rt.spec
tree RPMS
# Remove packages we don't want
# Not that using "--without debuginfo" prevented the kernel to build (in the past at least)
rm -f RPMS/x86_64/kernel-rt-debuginfo* RPMS/x86_64/kernel-rt-kvm*
